## シーケンス図

void close() このレコーディング・ファイルを閉じて、それに関連するすべてのシステム・リソースを解放します。
boolean hasMoreEvents() レコーディング・ファイルに未読イベントが存在する場合true、それ以外の場合はfalseを返します。
static List<RecordedEvent> readAllEvents(Path path) ファイル内のすべてのイベントのリストを返します。
RecordedEvent readEvent() レコーディング中の次のイベントを読み込みます。
List<EventType> readEventTypes() このレコーディングのすべてのイベント・タイプのリストを返します。

```plantuml
autoactivate on
UserApp->RecordingFile
RecordingFile->RecordingInput 
note right
    メンバー変数としてRecordingInputを保持する
end note
RecordingFile->RecordingFile: findNext
note right
    ここ要チェック
end note
RecordingFile->RecordingFile: createChunkParser
RecordingFile->ChunkParser
note right
    ChunkParser のコンストラクタで色々やる
end note
ChunkParser->chunkHeader
RecordingFile->ChunkParser: readEvent
ChunkParser-->RecordingFile: RecordedEvent
note right
ChunkParserのreadEventについては、別途シーケンスを起こす
end note
```

### RecoridingFileの持つメンバー変数（RecordingInput input)の初期化の流れ

```plantuml
autoactivate on
RecordingInput -> RecordingInput
RecordingInput -> currentBlock
RecordingInput -> previousBlock
RecordingInput -> RecordingInput : initialize
RecordingInput -> currentBlock : reset
RecordingInput -> previousBlock : reset
```

### ChunkParserのコンストラクタ

```plantuml
participant RecordingFile
participant ChunkParser
participant RecordedEvent
participant ChunkHeader
participant RecordingInput
activate RecordingInput
collections ConstantLookups

RecordingFile->ChunkParser 
activate ChunkParser
ChunkParser -> RecordedEvent
activate RecordedEvent
note right
    JdkJfrConsumerのnewRecordedEventで
    デフォルトのRecordedEventを生成してメンバー変数として保持する
end note
ChunkParser -> ChunkHeader
activate ChunkHeader
note right 
    RecordingInputを引数にChunkHeaderを生成
end note
ChunkHeader->RecordingInput : setValidSize
ChunkHeader->RecordingInput : position
ChunkHeader->RecordingInput : verifyMagic
note right
FLR\0で始まっているかをチェックする
end note
ChunkHeader->RecordingInput : readRawShort

note right
    2バイト読み込んでmajor番号を取得
    他にもheader情報を読み込んで設定
end note
ChunkHeader --> ChunkParser : ChunkHeader
deactivate
ChunkParser -> ConstantLookups 
activate ConstantLookups
ChunkParser -> ChunkHeader : readMetadata
ChunkParser -> TimeConverter : Metadataの値で初期化
activate TimeConverter
ChunkParser -> ParserFactory : ParserFactory(Metadata, ConstantLookups, TimeConverter)
activate ParserFactory
note right
    ParserFactoryのシーケンスは別紙参照
end note
ChunkParser -> ParserFactory : getParsers
ChunkParser -> ParserFactory : getTypeMap
ChunkParser -> ChunkParser : updateConfiguration

ChunkParser -> ConstantLookups : ConstantLookups内の各要素のnewPoolを実行（枠を作っているだけ）
ChunkParser -> ChunkParser : fillConstantPools 
activate ChunkParser
note right
    fillConstantPoolsのシーケンスは別紙参照
end note
deactivate ChunkParser
ChunkParser -> ConstantLookups : 各要素の getLatestPool().setResolving()を実行
ChunkParser -> ConstantLookups : 各要素の getLatestPool().resolve() を実行
ChunkParser -> ConstantLookups : 各要素の getLatestPool().setResolved() を実行
ChunkParser -> RecordingInput : positionをchunkHeader.getEventStartに変更
ChunkParser --> RecordingFile : ChunkParser
```

#### ParserFactoryのシーケンス作成

```plantuml
participant ChunkParser
participant ParserFactory
collections Parsers
participant MetadataDescriptor
participant TimeConverter
activate TimeConverter
collections ConstantLookups
activate ConstantLookups
collections Types
collections FiledのParser
participant CompositeParser
participant Typeに応じたParser

note over Parsers
    Typeに応じたParserを管理する
    ParserFactoryのメンバー
end note

ChunkParser -> ParserFactory
activate ParserFactory
ParserFactory -> Types: MetadataDescriptorの各TypeのIDとTypeをセット

loop Typesの全要素
    ParserFactory -> Types : getType
    Types --> ParserFactory : Type
    ParserFactory -> ParserFactory : createCompositeParser
    activate ParserFactory
    alt フィールドを持っている場合
        ParserFactory -> FiledのParser
        activate FiledのParser

        ParserFactory -> CompositeParser : FiledのParserを管理するCompositeParserのインスタンスを生成
        activate CompositeParser

        ParserFactory -> ParserFactory : registerParserType
        activate ParserFactory
        ParserFactory -> Parsers : put
        note over Parsers
            該当するTypeのParserが未登録であればput
        end note
        deactivate ParserFactory


        loop Fieldの数分
            ParserFactory -> ParserFactory : createParser
            activate ParserFactory
            ParserFactory -> Typeに応じたParser : 生成
            activate Typeに応じたParser
            note over ParserFactory
                Typeに応じたParserの返却タイプは以下
                - 配列の場合:ArrayParser
                - constantPoolの場合: ConstantValueParser（詳細は別シーケンス参照）
                - EventParserから呼ばれている場合: EventValueConstantParser
                - 上記i以外
                    - Filedを持っている場合：CompositeParser
                    - Filedを持っていない場合： PrimitiveParser（型ごとにある）
                        int,long,float,double,char,boolean,short,byte
                        java.lang.String
            end note

            deactivate ParserFactory

        end
    end 

end

loop metadata.getEventTypes
    ParserFactory -> ParserFactory : createEventParser
    activate ParserFactory
    ParserFactory -> EventParser
    activate EventParser
    deactivate ParserFactory
    ParserFactory -> Parsers : put
end loop
ParserFactory --> ChunkParser

```

##### ConstantValueParser（詳細は別シーケンス参照）の生成箇所の補足シーケンス

ConstantPoolがtrueの場合のみ実行されるブロック

```plantuml
participant ParserFactory
activate ParserFactory
participant Typeに応じたParser
collections constantLookups
activate constantLookups
participant Typeに応じたObjectFactory
note over Typeに応じたObjectFactory
    以下のType以外はnullが設定
    java.lang.Thread
    **PREFIX**.StackFrame
    **PREFIX**.Method
    **PREFIX**.ThreadGroup
    **PREFIX**.StackTrace
    **PREFIX**.ClassLoader
    java.lang.Class
    ※**REFIX**は、jdk.types または、com.oralce.jfr
end note
collections Typeに応じたConstantMap
collections ConstantLookup

ParserFactory -> ParserFactory : createParser
activate ParserFactory

ParserFactory -> Typeに応じたParser : 生成

ParserFactory -> Typeに応じたObjectFactory
activate Typeに応じたObjectFactory
ParserFactory -> Typeに応じたConstantMap
    note right
        生成したObjectFactoryとTypeをConstantMapで管理する
    end note
activate Typeに応じたConstantMap
ParserFactory -> ConstantLookup
note right
    生成したConstantMapと、Typeを管理する
end note
ParserFactory -> constantLookups : 登録
note right
    TypeのIDで生成したConstantLookupを管理する
end note

alt Event型の場合
    ParserFactory -> EventValueConstantParser : ConstantLookupを引数に生成
else Event型以外の場合
    ParserFactory -> ConstantValueParser : ConstantLookupを引数に生成
end 

ParserFactory -> ParserFactory
note over ParserFactory
    生成したParser(EventValueConstantParser または ConstantValueParser)
end note
deactivate ParserFactory
```

#### fillConstantPoolのシーケンス作成

```plantuml
participant ChunkParser
activate ChunkParser

participant chunkHeader
activate chunkHeader

collections Parsers
activate Parsers

participant IDに対応するParser
activate IDに対応するParser

participant RecordingInput
activate RecordingInput

collections constantLookups
activate constantLookups
collections ConstantLookup


ChunkParser -> ChunkParser : fillConstantPool(long abortCP) abortCP は0で指定している
activate ChunkParser

loop 読み込み位置が指定されたabortCPまで行くか、delta が0になったら終了

    ChunkParser -> chunkHeader : getConstantPoolPosition
    ChunkParser -> chunkHeader : getAbsoluteChunkStart
    ChunkParser -> RecordingInput : position 
    note over ChunkParser
        ConstantPoolの読み込み位置※へ移動する

        ※getAbsoluteChunkStartとgetConstantPoolPositionの値を足して
          ConstantPoolが読み込み位置を計算して算出する。
    end note
    ChunkParser -> RecordingInput
    note right
        size,typeId,timestamp,duration,
        delta,flush,poolCountなどの情報を読み込む
    end note

    loop poolcount数文だけ読み込む
        ChunkParser -> RecordingInput : idを読み込み
        ChunkParser -> constantLookups : idに対応するlookup
        ChunkParser -> ConstantLookup : idを指定して生成
        activate ConstantLookup
        ChunkParser -> constantLookups : put(id, ConstantLookup)
        ChunkParser -> Parsers 
        note over Parsers
           idに対応するTypeのParserを返却
        end note
 
        ChunkParser -> RecordingInput : read count
        
        loop count数文parse
            ChunkParser -> RecordingInput : keyを読み込み
            ChunkParser -> IDに対応するParser : parse(input)
            activate IDに対応するParser
                IDに対応するParser -> RecordingInput : 色々データを読み込み
                IDに対応するParser --> ChunkParser : parseして生成したオブジェクト
            deactivate IDに対応するParser
            ChunkParser -> ConstantLookup :  keyと、生成したオブジェクトの対応を追加

        end 
        

    end  

end
```

#### RecordingFile.readEventのシーケンス作成

https://docs.oracle.com/javase/jp/17/docs/api/jdk.jfr/jdk/jfr/consumer/RecordingFile.html


```plantuml

participant UserAP
activate UserAP

participant RecordingFile
activate RecordingFile

participant ChunkParser
activate ChunkParser

participant "該当するTypeの\nEventParser"
activate "該当するTypeの\nEventParser"

collections "ChunkParserの保持する\nparser"
activate "ChunkParserの保持する\nparser"

collections "EventParserの\n各フィールドのparser" as EventParserFiledParser
activate EventParserFiledParser

participant FiledのParser

participant RecordingInput
activate RecordingInput

participant JdkJfrConsumer
activate JdkJfrConsumer

participant RecordedEvent

UserAP -> RecordingFile : new
UserAP -> RecordingFile : readEvent
RecordingFile -> ChunkParser : readEvent

ChunkParser -> RecordingInput : typeIDを読みこみ

ChunkParser -> "ChunkParserの保持する\nparser" : 該当するparserを取得

ChunkParser ->  "該当するTypeの\nEventParser" : parse
note over of  "該当するTypeの\nEventParser"
    startTicks
    durationTicks 
    // endTicks は startTicks + durationTicksで計算する
    ここで、時間のフィルタをかけている

    各種フィールドのparseを実施して読み込む
end note

loop 
     "該当するTypeの\nEventParser" -> EventParserFiledParser: FiledのParserを取得
     "該当するTypeの\nEventParser" -> FiledのParser : parse
    activate FiledのParser

    FiledのParser -> RecordingInput : parseを行う
    FiledのParser --> "該当するTypeの\nEventParser" : 各フィールドの読み込み結果
    note right
        対応する型のIDとindex
        または直値(Stringなど)
    end note
    deactivate FiledのParser

end 

     "該当するTypeの\nEventParser" -> JdkJfrConsumer : newRecordedEvent

    JdkJfrConsumer -> RecordedEvent : 生成

activate RecordedEvent

JdkJfrConsumer --> "該当するTypeの\nEventParser" : 生成したRecordedEvent

 "該当するTypeの\nEventParser" --> ChunkParser : RecordedEvent

ChunkParser --> RecordingFile : RecordedEvent
RecordingFile --> UserAP : RecordedEvent

```

#### Eventの文字列化

https://docs.oracle.com/javase/jp/11/docs/api/jdk.jfr/jdk/jfr/consumer/RecordedEvent.html

```plantuml
class RecordedEvent {
    + getStackTrace イベントの継続時間をナノ秒単位で返します。
    + getThread     イベントの終了時間を返します。
    + getEventType  イベントを記述するイベント・タイプを返します。
    + getStartTime  イベントのフィールドを説明するディスクリプタのリストを返します。
    + getEndTime    イベントがコミットされたときに作成されたスタック・トレースを返します。イベントにスタック・トレースがない場合はnullを返します。
    + getDuration   イベントの開始時間を返します。
    + getFields     イベントがコミットされたスレッドを返します。スレッドが記録されていない場合はnullを返します。
}   
```

getBoolean以下は、 jdk.jfr.consumer.RecordedObjectの継承オブジェクト

|返却値| メソッド名| 概要|
|--|--|--|
| Duration | 	getDuration() |イベントの継続時間をナノ秒単位で返します。|
|Instant|getEndTime()|イベントの終了時間を返します。|
|EventType|getEventType()|イベントを記述するイベント・タイプを返します。|
|List\<ValueDescriptor\>|getFields()|イベントのフィールドを説明するディスクリプタのリストを返します。|
|RecordedStackTrace|getStackTrace()|イベントがコミットされたときに作成されたスタック・トレースを返します。イベントにスタック・トレースがない場合はnullを返します。|
|Instant|getStartTime()|イベントの開始時間を返します。|
|RecordedThread|getThread()|イベントがコミットされたスレッドを返します。スレッドが記録されていない場合はnullを返します。|
|boolean|getBoolean​(String name)|boolean型のフィールドの値を返します。|
|byte|getByte​(String name)|byte型のフィールドの値を返します。|
|char|getChar​(String name)|char型のフィールドの値を返します。|
|RecordedClass|getClass​(String name)|Class型のフィールドの値を返します。|
|double|getDouble​(String name)|広範な変換によってdouble型のフィールド、またはdouble型に変換可能な別のプリミティブ型のフィールドの値を返します。|
|List \<ValueDescriptor\>|getFields()|このオブジェクトのフィールドの不変なリストを返します。|
|float|getFloat​(String name)|広範な変換によってfloat型のフィールド、またはfloat型に変換可能な別のプリミティブ型のフィールドの値を返します。|
|Instant|getInstant​(String name)|タイムスタンプ・フィールドの値を返します。|
|int|getInt​(String name)|広範な変換によってint型のフィールド、またはint型に変換可能な別のプリミティブ型のフィールドの値を返します。|
|long|getLong​(String name)|広範な変換によってlong型のフィールド、またはlong型に変換可能な別のプリミティブ型のフィールドの値を返します。|
|short|getShort​(String name)|広範な変換によってshort型のフィールド、またはshort型に変換可能な別のプリミティブ型のフィールドの値を返します。|
|String|getString​(String name)|String型のフィールドの値を返します。|
|RecordedThread|getThread​(String name)|Thread型のフィールドの値を返します。|
|\<T\> T|getValue​(String name)|指定された名前のフィールドの値を返します。|
|boolean|hasField​(String name)|指定された名前のフィールドが存在する場合はtrueを返し、そうでない場合はfalse。|
|String|toString()|このオブジェクトのテキスト表現を返します。|

ChunkHeaderを見ると固定値がわかる

```java
    public static final long HEADER_SIZE = 68;
    static final byte UPDATING_CHUNK_HEADER = (byte) 255;
    public static final long CHUNK_SIZE_POSITION = 8;
    static final long DURATION_NANOS_POSITION = 40;
    static final long FILE_STATE_POSITION = 64;
    static final long FLAG_BYTE_POSITION = 67;
    static final long METADATA_TYPE_ID = 0;
    static final byte[] FILE_MAGIC = { 'F', 'L', 'R', '\0' };
    static final int MASK_FINAL_CHUNK = 1 << 1;
```

## クラス図
### FileFormat

```plantuml

class Chunk {
    
}

class Class {
    xs:ID name
    xs:integer id
    xs:boolean simpleType
    superType
}

class 内部構造としてのmetadata{

}

class region {
    locale
    integer gmtOffset
}
note bottom
    locale="en"
    gmtOffset="32400000"※
    ※JSTに相当する値が設定されていた
end note

class Field {
    name
    xs:integer Classのid
    xs:boolean constantPool
    xs:integer dimension

}

class Filedのannotation{
    xs:integer Classのid
    value
}
note bottom
- class
  実際に見たサンプルでは以下のclassが使われていたが、
  すべてjava.lang.annotation.Annotationの拡張クラスだった。
  jdk.jfr.Label
  jdk.jfr.Timespan
  jdk.jfr.Description
  jdk.jfr.Timestamp
  jdk.jfr.DataAmount
  jdk.jfr.MemoryAddress
  jdk.jfr.Percentage
  jdk.jfr.Frequency
  jdk.jfr.Experimental
  jdk.jfr.Unsigned
  jdk.jfr.TransitionFrom
  jdk.types.JavaMonitorAddress
  jdk.types.SafepointId
  jdk.types.GcId
  jdk.types.CompileId
  jdk.types.SweepId
end note

class annotation{
    xs:integer Classのid
    value
}
note bottom
- class
  実際に見たサンプルでは以下のclassが使われていたが、
  すべてjava.lang.annotation.Annotationの拡張クラスだった。
    jdk.jfr.Name
    jdk.jfr.Label
    jdk.jfr.Timespan
    jdk.jfr.ContentType
    jdk.jfr.Description
    jdk.jfr.Category
    jdk.jfr.Experimental
    jdk.jfr.Relational
    jdk.jfr.BooleanFlag
- value
  ほとんどが１つだけの定義だが、複数の場合はvalue-0,1という形で定義されていた
end note

class Setting{
    name 
    xs:integer
    defaultValue
}
note bottom
    classは、jdk.settings.cutoffなどsetting用のクラスがあり、
    nameはclassに対する命名になっていた。
        jdk.settings.Enabled→enabled
        jdk.settings.Threshold→threshold
        jdk.settings.StackTrace→stackTrace
        jdk.settings.Period→period
        jdk.settings.Cutoff→cutoff
end note


Chunk o-- Header
Chunk o-- Metadata
Chunk o-- ConstantPool
Chunk o-- Events


Metadata o-- Root
Root o-- 内部構造としてのmetadata
Root o-- region
内部構造としてのmetadata o-- Class

Class o-- Field
Class o-- annotation
Class o-- Setting

Field  o-- Filedのannotation
```

### Parser

```plantuml

class Parser {
    
}

class ArrayParser {
    - Parser elementParser

    + parse(RecodingInput)
}
note bottom
    **__parseの処理概要__**
    sizeを読み取る
    size数だけelementParser.parse(RecodingInput)を繰り返す
end note

class CompositeParser {
    - Parser[] parsers
    
    + parse(RecodingInput)
}
note bottom of CompositeParser
    **__parseの処理概要__**
    parsers.lengthの数だけ
    parsers[X].parse(RecodingInput)を行う
end note

class EventValueConstantParser {
    - ConstantLookup lookup;
    - Object lastReferenceValue;

    + EventValueConstantParser(ConstantLookup lookup)
    + parse(RecodingInput)
}
note bottom of EventValueConstantParser
    **__parseの処理概要__**
    keyを読み込む
    lookup.getCurrentResolved(key)の値を返す
end note


class ConstantValueParser{
    - ConstantLookup lookup
    + parse(RecordingInput)
    + parseReferences(RecordingInput)
    + ConstantValueParser(ConstantLookup lookup)
}
note bottom of ConstantValueParser
 **__parseの処理概要__**
 inputからIDを読み込んで対応するオブジェクトを
 lookupから返却
end note

class BooleanParser {
    parse()
}
note top
    他に同様に以下がある
    ByteParser
    LongParser
    IntegerParser
    ShortParser
    CharacterParser
    FloatParser
    DoubleParser

end note


class StringParser {
    - ConstantLookup stringLookup
    - CharArrayParser charArrayParser
    - CharsetParser utf8parser
    - CharsetParser latin1parser
    - boolean event

    + StringParser(stringLookup, event)
    + parse(RecordingInput)
}
note top 
    最初にEncodeが何かをreadByteする。読み込んだ値によって以下の処理を実行する
        0: NULL
           nullを返却する
        1: EMPTY_STRING
           空文字 "" を返却する
        2: CONSTANT POOL
           keyを取得(readLong)して、keyに対応する値をへkにゃくする
              eventのStringParser場合 ：stringLookup.getCurrentResolved(key);
              eventではない場合       ：stringLookup.getCurrent(key);
        3: UT8_BYTE_ARRAY
           utf8parser.parse(input)の結果を返す
        4: CHAR_ARRAY
           charArrayParser.parse(input)の結果を返す
        5: LATIN1_BYTE_ARRAY
           latin1parser.parse(input)の結果を返す
end note

Parser <|--- ArrayParser
Parser <|--- CompositeParser
Parser <|--- EventValueConstantParser

Parser <|-- BooleanParser
Parser <|-- StringParser
Parser <|--- ConstantValueParser

```

### CConstantLookup/ConstantMap

```plantuml

ConstantLookup o-- ConstantMap

class ConstantLookup {
    - Type type
    - ConstantMap current
    - ConstantMap previous

    + ConstantLookup(ConstantMap current, Type type)
    + Type getType() 
    + ConstantMap getLatestPool() 
    + void newPool() //新しいConstatMapを作成して、currentは一番作成したConstantMapになる。※利用箇所を見つけてから整理しなおす
    + Object getPreviousResolved(long key) 
    + Object getCurrentResolved(long key) : current.getResolved(key)を実行する
    + Object getCurrent(long key) 
}
note top
    ConstantMapの連結リストの最新要素を指す
end note

class ConstantMap{
    - Type type
    - LongMap<Object> objects

    + ConstantMap(ObjectFactory<?> factory, Type type)
    + void put(long key, Object value)
    + Object getResolved(long id)
    + Object get(long id) 
}
note bottom
    getResolvedは、putしたkeyに対応する値を返却する
end note

```
