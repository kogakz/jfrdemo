package com.inspection;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

import jdk.jfr.consumer.RecordedEvent;
import jdk.jfr.consumer.RecordingFile;

/**
 * Hello world!
 *
 */
public class JfrDemo {
    public static void main(String[] args) throws IOException {
        if (args.length != 1) {
            System.err.println("Must specify a recording file.");
            return;
        }

        RecordingFile recordingFile = new RecordingFile(Path.of(args[0]));

        System.out.println("ID,Name,Label");
        while (recordingFile.hasMoreEvents()) {
            RecordedEvent event = recordingFile.readEvent();
            System.out.println(event.getEventType().getId() + "," + event.getEventType().getName() + ","
                    + event.getEventType().getLabel());

        }

    }
}