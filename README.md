# 

## OopeJDKのビルド

```
git clone https://git.openjdk.org/jdk/


sudo apt update
sudo apt install -y file
sudo apt install -y autoconf

wget https://download.java.net/java/GA/jdk20.0.1/b4887098932d415489976708ad6d1a4b/9/GPL/openjdk-20.0.1_linux-x64_bin.tar.gz
tar -xvzf openjdk-20.0.1_linux-x64_bin.tar.gz
rm openjdk-20.0.1_linux-x64_bin.tar.gz
export JAVA_HOME=`pwd`\jdk-20.0.1
export PATH=`pwd`\jdk-20.0.1/bin:$PATH
git clone https://git.openjdk.org/jdk/
cd jdk
bash configure --build=x86_64-unknown-linux-gnu --openjdk-target=x86_64-unknown-linux-gnu

#sudo apt install -y clang
sudo apt install -y gcc
sudo apt install -y unzip
sudo apt install -y zip
sudo apt-get install libfreetype6-dev

export PATH=`pwd`\jdk-20.0.1/bin:$PATH


PATH=/home/ssm-user/jdk-20.0.1/bin:$PATH
PATH=/home/ssm-user/jdk-20.0.1/bin:$PATH
JAVA_HOME=/home/ssm-user/jdk-20.0.1



# ビルドする
## 参考サイト https://github.com/openjdk/jtreg/blob/master/doc/building.md
## WSLの場合は --openjdk-target=x86_64-unknown-linux-gnu が必要
bash configure --build=x86_64-unknown-linux-gnu --openjdk-target=x86_64-unknown-linux-gnu
make images

# テスト付きで実行する場合
# すごく時間かかった。もうやらない。
bash configure --build=x86_64-unknown-linux-gnu --openjdk-target=x86_64-unknown-linux-gnu --with-jtreg
make images
make run-test-tier1
make clean

# デバッグオプション付き
bash configure --build=x86_64-unknown-linux-gnu --openjdk-target=x86_64-unknown-linux-gnu --enable-debug
```

## 




